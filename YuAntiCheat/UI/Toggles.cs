namespace YuAntiCheat;

public class Toggles
{
    // 界面
    public static bool DarkMode = true;

    // Ping部分
    public static bool ShowCommit = true;
    public static bool ShowModText = true;
    public static bool ShowIsSafe = true;
    public static bool ShowSafeText = true;
    public static bool ShowIsDark = true;
    public static bool ShowPing = true;
    public static bool ShowFPS = true;
    public static bool ShowIsAutoExit = true;
    public static bool ShowRoomTime = true;
    public static bool ShowUTC;
    public static bool ShowLocalNowTime = true;

    // 反作弊
    public static bool SafeMode = true;
    public static bool AutoExit;

    // 快捷按钮
    public static bool DumpLog;
    public static bool OpenGameDic;
    
    //玩法性-所有人
    public static bool ExitGame;
    public static bool RealBan;

    // 仅房主
    public static bool ChangeDownTimerToZero;
    public static bool AbolishDownTimer;
    public static bool AutoStartGame;
    //玩法性-房主
    public static bool ChangeDownTimerTo114514;

    // 其他
    public static bool FPSPlus = true;
    
}